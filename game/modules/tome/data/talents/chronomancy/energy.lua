-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2014 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

-- EDGE TODO: Particles, Timed Effect Particles

newTalent{
	name = "Energy Decomposition",
	type = {"chronomancy/energy",1},
	mode = "sustained",
	require = chrono_req_high1,
	points = 5,
	sustain_paradox = 24,
	cooldown = 10,
	tactical = { BUFF = 2 },
	getDecomposition = function(self, t) return  self:combatTalentSpellDamage(t, 5, 150, getParadoxSpellpower(self, t)) end, -- Increase shield strength
	callbackOnTakeDamage = function(self, t, hd)
		local decomp = t.getDecomposition(self, t)

		-- works like armor with 30% hardiness
		local dam = hd.dam
		dam = math.max(dam * 0.3 - decomp, 0) + (dam * 0.7)
		print("[PROJECTOR] after static reduction dam", dam)
		game:delayedLogDamage(hd.src or self, hd.self, 0, ("%s(%d dissipated)#LAST#"):format(DamageType:get(type).text_color or "#aaaaaa#", hd.dam - dam), false)
		hd.dam = dam
		return true
	end,
	activate = function(self, t)
		game:playSoundNear(self, "talents/heal")
		return {
			particle = self:addParticles(Particles.new("temporal_focus", 1)),
		}
	end,
	deactivate = function(self, t, p)
		self:removeParticles(p.particle)
		return true
	end,
	info = function(self, t)
		local decomp = t.getDecomposition(self, t)
		return ([[Partially dissipates all incoming damage, reducing it by 30%%, up to a maximum of %d.
		The maximum damage reduction will scale with your Spellpower.]]):format(decomp)
	end,
}

newTalent{
	name = "Energy Absorption",
	type = {"chronomancy/energy", 2},
	require = chrono_req_high2,
	points = 5,
	paradox = function (self, t) return getParadoxCost(self, t, 10) end,
	cooldown = 6,
	fixed_cooldown = true,
	tactical = { DISABLE = 2 },
	direct_hit = true,
	requires_target = true,
	range = 10,
	getTalentCount = function(self, t)
		return 1 + math.floor(self:combatTalentLimit(t, 3, 0, 2))
	end,
	getDamage = function(self, t) return self:combatTalentSpellDamage(t, 20, 80, getParadoxSpellpower(self, t)) end,
	getCooldown = function(self, t) return math.ceil(self:combatTalentScale(t, 1, 2.6)) end,
	target = function (self, t)
		return {type="hit", range=self:getTalentRange(t), talent=t}
	end,
	action = function(self, t)
		local tg = self:getTalentTarget(t)
		local tx, ty = self:getTarget(tg)
		if not tx or not ty then return nil end
		local _ _, tx, ty = self:canProject(tg, tx, ty)
		local target = game.level.map(tx, ty, Map.ACTOR)
		if not target then return end

		if not self:checkHit(getParadoxSpellpower(self, t), target:combatSpellResist()) then
			game.logSeen(target, "%s resists!", target.name:capitalize())
			return true
		end

		local tids = {}
		for tid, lev in pairs(target.talents) do
			local t = target:getTalentFromId(tid)
			if t and not target.talents_cd[tid] and t.mode == "activated" and not t.innate then tids[#tids+1] = t end
		end

		local count = 0
		local cdr = t.getCooldown(self, t)

		for i = 1, t.getTalentCount(self, t) do
			local t = rng.tableRemove(tids)
			if not t then break end
			target.talents_cd[t.id] = cdr
			game.logSeen(target, "%s's %s is disrupted by the Energy Drain!", target.name:capitalize(), t.name)
			count = count + 1
		end

		if count >= 1 then
			local tids = {}
			for tid, _ in pairs(self.talents_cd) do
				local tt = self:getTalentFromId(tid)
				if tt.type[1]:find("^chronomancy/") and not tt.fixed_cooldown then
					tids[#tids+1] = tt
				end
			end
	
			for i = 1, count do
				if #tids == 0 then break end
				local tid = rng.tableRemove(tids)
				self:alterTalentCoolingdown(tid, - cdr)
			end
			
			-- Deal our damage in one lump sum
			self:project(tg, target.x, target.y, DamageType.TEMPORAL, self:spellCrit(count * t.getDamage(self, t)))
		end
		
		target:crossTierEffect(target.EFF_SPELLSHOCKED, getParadoxSpellpower(self, t))
		game.level.map:particleEmitter(tx, ty, 1, "generic_charge", {rm=10, rM=110, gm=10, gM=50, bm=20, bM=125, am=25, aM=255})
		game.level.map:particleEmitter(self.x, self.y, 1, "generic_charge", {rm=200, rM=255, gm=200, gM=255, bm=200, bM=255, am=125, aM=125})
		game:playSoundNear(self, "talents/spell_generic")
		return true
	end,
	info = function(self, t)
		local talentcount = t.getTalentCount(self, t)
		local cooldown = t.getCooldown(self, t)
		local damage = t.getDamage(self, t)
		return ([[You sap the target's energy and add it to your own, placing up to %d random talents on cooldown for %d turns.  
		For each talent put on cooldown, you reduce the cooldown of one of your chronomancy talents currently on cooldown by %d turns and deal %0.2f temporal damage to the target.
		The damage done will scale with your Spellpower.]]):
		format(talentcount, cooldown, cooldown, damDesc(self, DamageType.TEMPORAL, damage))
	end,
}

newTalent{
	name = "Redux",
	type = {"chronomancy/energy",3},
	require = chrono_req_high3,
	points = 5,
	paradox = function (self, t) return getParadoxCost(self, t, 20) end,
	cooldown = 24,
	tactical = { BUFF = 2 },
	fixed_cooldown = true,
	getDuration = function(self, t) return getExtensionModifier(self, t, math.floor(self:combatTalentScale(t, 2, 4))) end,
	getMaxCooldown = function(self, t) return math.floor(self:combatTalentScale(t, 3, 8)) end,
	action = function(self, t)
		-- effect is handled in actor postUse
		self:setEffect(self.EFF_REDUX, t.getDuration(self, t), {max_cd=t.getMaxCooldown(self, t)})
		game:playSoundNear(self, "talents/heal")
		return true
	end,
	info = function(self, t)
		local duration = t.getDuration(self, t)
		local cooldown = t.getMaxCooldown(self, t)
		return ([[For the next %d turns chronomancy spells with a cooldown of %d or less have a cooldown of one.]]):
		format(duration, cooldown)
	end,
}

newTalent{
	name = "Entropy",
	type = {"chronomancy/energy",4},
	require = chrono_req_high4,
	points = 5,
	paradox = function (self, t) return getParadoxCost(self, t, 20) end,
	cooldown = 12,
	tactical = { ATTACK = { TEMPORAL = 2 }, DEBUFF=3 },
	range = 10,
	getDamage = function(self, t) return self:combatTalentSpellDamage(t, 20, 100, getParadoxSpellpower(self, t)) end,
	getDuration = function(self, t) return getExtensionModifier(self, t, math.floor(self:combatTalentScale(t, 3, 7))) end,
	target = function(self, t)
		return {type="hit", range=self:getTalentRange(t), talent=t}
	end,
	requires_target = true,
	direct_hit = true,
	action = function(self, t)
		local tg = self:getTalentTarget(t)
		local x, y, target = self:getTarget(tg)
		if not x or not y or not target then return nil end
		local _ _, x, y = self:canProject(tg, x, y)
		
		local damage = self:spellCrit(t.getDamage(self, t))
		target:setEffect(target.EFF_ENTROPY, t.getDuration(self, t), {power=damage, src=self, apply_power=getParadoxSpellpower(self, t)})

		game:playSoundNear(self, "talents/dispel")

		return true
	end,
	info = function(self, t)
		local damage = t.getDamage(self, t)
		local duration = t.getDuration(self, t)
		return ([[Over the next %d turns all beneficial timed effects on the target tick twice as fast.
		Each timed effect affected by this talent deals %0.2f temporal damage to the target.
		The damage will scale with your Spellpower.]]):format(duration, damDesc(self, DamageType.TEMPORAL, damage))
	end,
}